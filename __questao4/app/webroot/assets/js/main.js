
var result;

function getTasks (id)
{
	data = '';
	$.ajax({
	    type: 'GET',
	    url: APP_ROOT+'api/get',
	    data: data,
	    dataType: 'JSON',
	    success: function(res) {
            if (res.status=='ok') {
            	$('#tasks-list').fadeOut();
            	$('#tasks-list').empty();
            	$.each(res.results, function(i, item) {
            		if(i==0){ i++; }
            		var li = '';
            		li+= '<li class="list-group-item" rel="'+item.Tasks.id+'">';
            		li+= '<div class="row">';
            		li+= '<div class="col-lg-10">';
            		li+= '<div class="box"><span class="badge">'+i+'</span></div>';
            		li+= '<div class="box">';
            		li+= item.Tasks.titulo+' <br>';
            		li+= item.Tasks.descricao;
            		li+= '</div>';
            		li+= '</div>';
            		li+= '<div class="col-lg-2 text-right">';
            		li+= '<button type="button" class="btn btn-sm btn-primary" onclick="setEdit(\''+item.Tasks.titulo+'\',\''+item.Tasks.descricao+'\',\''+item.Tasks.id+'\')" data-toggle="modal" data-target="#editModal">Editar</button>';
            		li+= '<button type="button" class="btn btn-sm btn-danger" onclick="deteleTask('+item.Tasks.id+')">Excluir</button>';
            		li+= '</div>';
            		li+= '</div>';
            		li+= '</li>';
            		$('#tasks-list').append(li);
            	});
				$('#tasks-list').fadeIn();
				$('.rows').empty();
				$('.rows').append('Olá, você possui <strong>'+res.rows+'</strong> tarefa(s) em sua lista.');
            }
        }
	});
}

function addTask ()
{

	var titulo = $('input[name="data[Tasks][titulo]"').val();
	var descricao = $('textarea[name="data[Tasks][descricao]"').val();

	$.ajax({
	    type: 'POST',
	    url: APP_ROOT+'api/add',
	    dataType: 'JSON',
	    data: {titulo:titulo, descricao:descricao},
	    success: function(res) {
            if (res.status=='ok') {
            	$('.add_ok').fadeIn().delay(800).fadeOut();
            	$('textarea[name="data[Tasks][descricao]"').val('');
            	$('input[name="data[Tasks][titulo]"').val('')
            } else {
            	$('.add_error').fadeIn().delay(800).fadeOut();
            }
        }
	}).done(function(){
		getTasks();
	});
	
}

function updateTask ()
{
	var titulo = $('input[name="data[Edit][titulo]"').val();
	var descricao = $('textarea[name="data[Edit][descricao]"').val();
	var id = $('input[name="data[Edit][id]"').val();

	console.log(id);

	$.ajax({
	    type: 'POST',
	    url: APP_ROOT+'api/update/'+id,
	    dataType: 'JSON',
	    data: {titulo:titulo, descricao:descricao},
	    success: function(res) {
            if (res.status=='ok') {
            	$('.edit_ok').fadeIn().delay(800).fadeOut();
            	$('textarea[name="data[Tasks][descricao]"').val('');
            	$('input[name="data[Tasks][titulo]"').val('')
            } else {
            	$('.edit_error').fadeIn().delay(800).fadeOut();
            }
        }
	}).done(function(aaaa){
		getTasks();
	});
	
}

function deteleTask (id)
{
	$.ajax({
	    type: 'POST',
	    url: APP_ROOT+'api/delete/'+id,
	    dataType: 'JSON',
	    data: null,
	    success: function(res) {
            if (res.status=='ok') {
            	$('.delete_ok').fadeIn().delay(800).fadeOut();
            } else {
            	$('.delete_error').fadeIn().delay(800).fadeOut();
            }
        }
	}).done(function(aaaa){
		getTasks();
	});
}


getTasks();


function setEdit(titulo,descricao,id){
	$('input[name="data[Edit][titulo]"').empty().val(titulo);
	$('textarea[name="data[Edit][descricao]"').empty().val(descricao);
	$('input[name="data[Edit][id]"').empty().val(id);
}