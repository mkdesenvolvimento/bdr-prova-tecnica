<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php echo $this->Html->charset() ?>
    <title>
        <?php echo isset($seo['title']) ? $seo['title'] : ''; ?>
    </title>

    <?php echo $this->Html->meta('favicon.ico','/assets/images/favicon.png',array('type' => 'icon')) ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <script type="text/javascript">
        var APP_ROOT = "<?php echo $this->Html->Url('/', true); ?>";    
    </script>

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/bootstrap.min.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/style.css?nocache='.time(), true); ?>">     

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">

    <?php
        echo $this->Html->script('/assets/js/lib/jquery-1.12.0.min');
        echo $this->Html->script('/assets/js/lib/jquery-ui');
        echo $this->Html->script('/assets/js/lib/bootstrap.min');
    ?>
</head>

<body>       

    <div class="container">
        <div id="header" class="row">
            <div class="col-lg-12">
                <h2>Lista de Tarefas</h2>
            </div>
            <div class="col-lg-12">
                <hr />
                <div class="row">
                    <div class="col-lg-6 rows">
                        
                    </div>
                    <div class="col-lg-6 text-right">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal">Adicionar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="alert alert-success delete_ok">
                        Sucesso! Sua tarefa foi excluida com sucesso.
                    </div>
                    <div class="alert alert-danger delete_error">
                        Falha! Ocorreu um erro ao excluir sua tarefa.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-10">
                        <strong>Prioridade / Tarefa</strong>
                    </div>
                </div>
                <ul id="tasks-list" class="list-group">
                    
                    <li class="list-group-item">
                        Carregando...
                    </li>
                    
                </ul>

            </div>
        </div>
    </div>


    <!-- Add Modal -->
    <div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Adicionar Tarefa</h4>
          </div>
          <div class="modal-body">
            <p>Adicione uma nova tarefa a sua lista.</p>
            <div class="form-group">
                <label for="">Titulo:</label>
                <input type="text" class="form-control" name="data[Tasks][titulo]" required="required">
            </div>
            <div class="form-group">
                <label for="">Descrição:</label>
                <textarea cols="30" rows="3" class="form-control" rel-title="Tarefa" rel-descri="Esta é uma descricao" placeholde="Digite uma descrição" required="required" name="data[Tasks][descricao]"></textarea>
            </div>
            <div class="alert alert-success add_ok">
                Sucesso! Sua tarefa foi adicionada ao final da lista.
            </div>
            <div class="alert alert-danger add_error">
                Falha! Ocorreu um erro ao adicionar sua tarefa.
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="addTask()">Adicionar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Edit Modal -->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar Tarefa</h4>
          </div>
          <div class="modal-body">
            <p>Altera uma tarefa de sua lista.</p>
            <div class="form-group">
                <label for="">Titulo:</label>
                <input type="text" class="form-control" name="data[Edit][titulo]" required="required">
                <input type="hidden" name="data[Edit][id]">
            </div>
            <div class="form-group">
                <label for="">Descrição:</label>
                <textarea cols="30" rows="3" class="form-control" name="data[Edit][descricao]" placeholde="Digite uma descrição" required="required"></textarea>
            </div>
            <div class="alert alert-success edit_ok">
                Sucesso! Sua tarefa foi atualizada com sucesso.
            </div>
            <div class="alert alert-danger edit_error">
                Falha! Ocorreu um erro ao adicionar sua tarefa.
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="updateTask()">Atualizar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div>

      </div>
    </div>

    
    <?php echo $this->fetch('content'); ?>

    <?php        
        echo $this->Html->script('/assets/js/main.js?'.time());
    ?>
    <script>
      $( function() {
        $( "#tasks-list" ).sortable();
        $( "#tasks-list" ).disableSelection();
      } );
      </script>
</body>

</html>