<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');


class ApiController extends AppController {

	public $uses = array('Tasks');

	private $returns = array(
		                      'nf' => 'Registro(s) não encontrado(s)', 
		                      'ip' => 'Parâmetro incorreto ou inválido',
		                      'wi' => 'Ocorreu um erro ao salvar a tarefa.',
		                      'ok' => 'Sua tarefa {NOME} foi inserida com sucesso.',
		                      'up' => 'Sua tarefa {NOME} foi atualizada com sucesso.',
		                      'de' => 'Tarefa excluída com sucesso.'
		                    );

	public function get ($id=null)
	{
		$this->autoRender = false;
		$this->layout = false;

		if ($this->request->is('get')) {
			if (isset($id)) {
				$results = $this->Tasks->findById($id);
			} else {
				$results = $this->Tasks->find('all',array(
													'order' => array('Tasks.prioridade ASC')
													)
										);
			}
			if (sizeof($results)>0) {
			    echo $this->getJson(array('status'=>'ok','results'=>$results));
			} else {
				echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['nf']));
			}
		} else {
			echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
		}
	}

	public function add ()
	{
		$this->autoRender = false;
		$this->layout = false;

		if ($this->request->is('post')) {
			$this->request->data['Tasks']['titulo'] = utf8_decode($this->request->data['titulo']);
			$this->request->data['Tasks']['descricao'] = utf8_decode($this->request->data['descricao']);
			if ($this->Tasks->save($this->request->data)) {
				echo $this->parseJson(array('status'=>'ok','msg'=>$this->returns['ok']));
			} else {
				echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['wi']));
			}

		} else {
			echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
		}
	}

	public function update ($id)
	{
		$this->autoRender = false;
		$this->layout = false;

		if ($this->request->is('post')) {
			if (isset($id)) {
				$this->Tasks->id = $id;
				if ($this->Tasks->exists()) {
					$this->request->data['Tasks']['titulo'] = utf8_decode($this->request->data['titulo']);
					$this->request->data['Tasks']['descricao'] = utf8_decode($this->request->data['descricao']);
					if ($this->Tasks->save($this->request->data)) {
				        echo $this->parseJson(array('status'=>'ok','msg'=>$this->returns['up']));
					} else {
						echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['wi']));
					}
				} else {
					echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['nf']));
				}
			} else {
				echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
			}
		} else {
			echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
		}
	}

	public function delete ($id)
	{
		$this->autoRender = false;
		$this->layout = false;

		if ($this->request->is('post')) {
			if (isset($id)) {
				$this->Tasks->id = $id;
				if ($this->Tasks->exists()) {
					if ($this->Tasks->delete($this->request->data)) {
				        echo $this->parseJson(array('status'=>'ok','msg'=>$this->returns['de']));
					} else {
						echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['wi']));
					}
				} else {
					echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['nf']));
				}
			} else {
				echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
			}
		} else {
			echo $this->parseJson(array('status'=>'error','msg'=>$this->returns['ip']));
		}
	}

	private function getJson(array $array){
		$return = array(
			'status' => isset($array['status']) ? $array['status'] : '',
			'msg' => isset($array['msg']) ? $array['msg'] : '',
			'rows' => count(isset($array['results']) ? $array['results'] : array()),
			'results' => isset($array['results']) ? $array['results'] : array()
		);
		return $this->parseJson($return);
	}

	private function parseJson (array $array) {
		// Devido a versão do PHP está é a solução poir o segundo parametro do json_encode só está disponível na versão 5.4+
		array_walk_recursive( $array, function(&$item) { 
		    $item = utf8_encode( $item ); 
		});
		return json_encode($array);
	}

}
