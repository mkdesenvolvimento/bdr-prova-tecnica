-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) DEFAULT NULL,
  `descricao` text,
  `prioridade` int(3) NOT NULL DEFAULT '999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tasks
-- ----------------------------
INSERT INTO `tasks` VALUES ('1', 'Tarefa 1', 'Descrição da tarefa 1', '999');
INSERT INTO `tasks` VALUES ('2', 'Tarefa 2', 'Descrição da tarefa 2', '999');
INSERT INTO `tasks` VALUES ('3', 'Tarefa 3', 'Descrição da tarefa 3', '999');
INSERT INTO `tasks` VALUES ('4', 'Tarefa 4', 'Descrição da tarefa 4', '999');
INSERT INTO `tasks` VALUES ('5', 'Tarefa 5', 'Descrição da tarefa 5', '999');
