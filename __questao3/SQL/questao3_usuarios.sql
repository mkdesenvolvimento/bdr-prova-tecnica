CREATE DATABASE  IF NOT EXISTS `questao3`;
USE `questao3`;

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=6;

LOCK TABLES `usuarios` WRITE;
INSERT INTO `usuarios` VALUES (1,'teste1'),(2,'teste2'),(3,'teste3'),(4,'teste4'),(5,'teste5');
UNLOCK TABLES;