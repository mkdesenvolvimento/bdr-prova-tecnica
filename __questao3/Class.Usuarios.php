<?php
require_once("Class.DatabaseConnection.php");

class Usuarios {

    public function getUsuariosList()
    {
    	$db = new DataBaseConnection();
    	$db->connectDb();
    	$db->query('SELECT * FROM usuarios ORDER BY usuarios.nome ASC');
    	$usuarios = $db->getResultArray();

    	return $usuarios;
    }

}

