<?php

class DataBaseConnection {

	private $user = 'root';
	private $password = 'root';
	private $host = 'localhost';
	private $database = 'questao3';
	private $connect;
	private $result_query;

    public function connectDb()
    {
    	$this->connect = mysql_connect($this->host,$this->user,$this->password);
    	mysql_select_db($this->database);
    }

	public function query($query)
	{
		if (isset($this->connect)) {
			$this->result_query = mysql_query($query,$this->connect);
		} else {
			$this->result_query = false;
		}
		mysql_close();
	}

	public function getResultArray()
	{
		$result = array();

		if ($this->result_query) {
			while($r=mysql_fetch_assoc($this->result_query)){
				$result[] = $r;
			}
		} else {
			return false;
		}
		return $result;
	}

}

