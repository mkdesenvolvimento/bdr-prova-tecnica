# README 

As questões estão separadas em seus diretorios, __questao1, __questao2, __questao3 e __questao 4.

# Questao 1 e 2
- Scripts refatorados e salvo no diretório indicado.

# Questao 3
- Refatorei o código e separei por classes, cheguei a criar uma tabela qual o script SQL encontra-se em /__questao3/SQL/
- Será necessário configurar os dados de acesso do banco no arquivo Class.DatabaseConnection.php

# Questao 4
- SQL com estrutura da tabela e nome do banco encontrado em /__questao4/SQL/
- Será necessário configurar os dados de acesso do banco no arquivo /core/database.php.default e renomea-lo para database.php (Como condiz com a documentação do CakePHP 2.4)
- O Framework utilizado foi o CakePHP 2.4 puro
- PHP versão 5.3.23 como solicitado no teste
