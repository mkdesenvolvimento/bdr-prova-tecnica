<?php

session_start();

function checkSession()
{
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
		return true;
	} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin']) {
		return true;
	}
	return false;
}

function redirect($url)
{
    if (isset($url)) {
    	header("Location: ".$url);
    } else {
    	return false;
    }
}


if (checkSession()) {
    redirect('http://www.google.com.br');
}

?>