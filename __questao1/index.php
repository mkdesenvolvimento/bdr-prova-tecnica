<?php

echo "<ul>";

for ($x=1; $x<=100; $x++) {

	echo '<li>';

    if ($x%3==0 && $x%5==0){
        echo '<span style="color:orange">FizzBuzz</span>';
    } elseif ($x%3==0) {
    	echo '<span style="color:blue">Fizz</span>';
    } elseif ($x%5==0) {
    	echo '<span style="color:red">Buzz</span>';
    } else {
        echo $x;
    }

    echo '</li>';

}

echo "</ul>";